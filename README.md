# TaskManager
A simple task-manager written in Python3 and Flask.
user can add a todo, mark it as done, edit and delete the task.
## to use this app:
- install python3, pip3, virtualenv
- clone the project
- create a virtualenv named venv using ```python3 -m venv venv```
- Connect to virtualenv using ```source venv/bin/activate```
- from the project folder, install packages using ```pip install -r requirements.txt```
- for db migrations use ```flask db upgrade```
- finally ```flask run```
## TODO:
- [x] some work on front-end
- [ ] add move option - up and down - for tasks
- [ ] edit task on main page