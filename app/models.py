from app import app
from app import db 

class Tasks(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    task = db.Column(db.String(150))
    status = db.Column(db.Boolean())